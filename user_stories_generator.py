import markovify

# Get raw text as string.
with open("/Users/j.fillioux/git/onepoint/ai4industry/markovify/dataset/fr/all_FR.txt") as f:
    text = f.read()

# Build the model.
text_model = markovify.NewlineText(text)

# Print five randomly-generated sentences
for i in range(10000):
    print(text_model.make_sentence())

# Print three randomly-generated sentences of no more than 280 characters
# for i in range(50):
#     print(text_model.make_short_sentence(280))